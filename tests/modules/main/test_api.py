# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_select_tags(client, user_session):
    """Test the internal "api.select_tags" endpoint."""
    with user_session():
        response = client.get(url_for("api.select_tags"))
        check_api_response(response)


def test_search_resources(client, user_session):
    """Test the internal "api.search_resources" endpoint."""
    with user_session():
        response = client.get(url_for("api.search_resources"))
        check_api_response(response)


def test_index(api_client, dummy_access_token):
    """Test the "api.index" endpoint."""
    response = api_client(dummy_access_token).get(url_for("api.index"))
    check_api_response(response)
