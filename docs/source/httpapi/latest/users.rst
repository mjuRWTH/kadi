Users
=====

GET
---

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.accounts.api
   :methods: get
   :autoquickref:
