Miscellaneous
=============

GET
---

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.main.api
   :methods: get
   :autoquickref:
