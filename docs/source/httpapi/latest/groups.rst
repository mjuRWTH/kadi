Groups
======

POST
----

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.groups.api
   :methods: post
   :autoquickref:

GET
---

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.groups.api
   :methods: get
   :autoquickref:

PATCH
-----

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.groups.api
   :methods: patch
   :autoquickref:

DELETE
------

.. autoflask:: kadi.wsgi:app
   :package: kadi.modules.groups.api
   :methods: delete
   :autoquickref:
