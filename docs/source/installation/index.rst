Installation
============

This chapter covers installing Kadi4Mat either for development or production
environments as well as how to install and configure plugins. The installation
has currently been tested under Debian 10 (Buster), which the installation
instructions are also based on. However, other current Debian-based
distributions like Ubuntu (>= 18) should work the same.

.. toctree::
   :maxdepth: 2

   Development <development/index>
   Production <production/index>
   Plugins <plugins>
