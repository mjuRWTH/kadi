.. _installation-development:

Development
===========

This section describes how to install Kadi4Mat in a development environment.
There are currently two three ways to do this: manual installation,
installation via Docker containers and a hybrid installation combining both
approaches. Manual installation, as well as the hybrid approach, include more
steps and also require installing most dependencies by hand. However, these
approaches offer more flexibility in terms of how the development environment
is set up and are therefore the recommended way of installation at the moment.

Regardless of the method of installation, the first step is always to obtain
the source code of Kadi4Mat using `git <https://git-scm.com>`__. For this, it
is recommended to create a fork of the `main repository
<https://gitlab.com/iam-cms/kadi>`__ first. Afterwards, the code can be cloned
into a local directory via SSH (recommended, see also the official `GitLab
documentation <https://docs.gitlab.com/ee/ssh/README.html>`__) or HTTPS. Note
that the *<username>* placeholder in the following command needs to be
substituted with the correct username/namespace that the new fork sits in:

.. code-block:: bash

  git clone git@gitlab.com:<username>/kadi.git ${HOME}/workspace/kadi

This will copy the code into the ``workspace/kadi`` directory in the current
user's home directory. This directory can of course be changed freely, however,
the rest of this documentation assumes that the source code resides there.

To be able to update the code from the central repository, it should be added
as an additional remote called *upstream* for example (note that the default
remote after cloning, pointing to the new fork, is always called *origin*):

.. code-block:: bash

  cd ${HOME}/workspace/kadi
  git remote add upstream git@gitlab.com:iam-cms/kadi.git

.. toctree::
   :maxdepth: 2

   Manual installation <manual>
   Installation via Docker <docker>
   Hybrid installation <hybrid>
