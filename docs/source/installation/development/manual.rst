.. _installation-development-manual:

Manual installation
===================

Manual installation is currently the recommended way of installation, as it
offers more flexibility in terms of how the development environment is set up
and will lead to a more *complete* environment. If you are interested in
running some of the listed dependencies in Docker containers, without losing
much flexibility, please refer to the :ref:`hybrid installation
<installation-development-hybrid>` as well.

.. include:: ../_dependencies.rst

Node.js
~~~~~~~

`Node.js <https://nodejs.org/en/>`__, including `npm
<https://www.npmjs.com/>`__, are used for managing the frontend dependencies
and building the asset bundles. They can be installed using:

.. code-block:: bash

  sudo apt install curl
  curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
  sudo apt install nodejs

Installing Kadi4Mat
-------------------

To create and activate a new virtual environment for the application, the
following commands can be used:

.. code-block:: bash

  virtualenv -p python3 ${HOME}/venvs/kadi
  source ${HOME}/venvs/kadi/bin/activate

This will create and activate a new virtual environment named ``kadi`` using
Python 3 as interpreter. The environment is stored inside the ``venvs``
directory in the current user's home directory. This directory can of course be
changed freely. For all following steps, the virtual environment is assumed to
be active. For an easier way to manage virtual environments, a tool like
:ref:`virtualenvwrapper <development-general-virtualenvwrapper>` can also be
helpful.

Afterwards, the application can be installed using the previously checked out
source code:

.. code-block:: bash

  pip install -e ${HOME}/workspace/kadi[dev]

This will install the application in editable mode, which simply creates a link
to the sources so all changes are reflected in the installed package
immediately. By specifying the ``[dev]`` modifier, all development dependencies
listed in :file:`setup.py` are installed as well.

At this point, it is also recommended to install the :ref:`pre-commit
<development-general-pre-commit>` hooks already by running:

.. code-block:: bash

  pre-commit install

Configuration
-------------

.. _installation-development-manual-configuration-postgres:

Postgres
~~~~~~~~

To set up Postgres, a user and a database belonging to that user have to be
created. When prompted for a password, use ``kadi``. This way, the default
configuration of the application does not need to be changed later on.

.. code-block:: bash

  sudo -u postgres createuser -P kadi
  sudo -u postgres createdb -O kadi kadi -E utf-8

.. _installation-development-manual-configuration-kadi4mat:

Kadi4Mat
~~~~~~~~

Some environment variables need to be set in order to use the application and
the correct configuration. Those can be set either directly on the command line
each time or in a :file:`.env` file (which needs to be created first),
preferably residing in the project's root directory (meaning
``${HOME}/workspace/kadi`` when following along with the example directory
structure). More details about this can be found in the `Flask Documentation
<https://flask.palletsprojects.com/en/1.1.x/cli/#environment-variables-from
-dotenv>`__.

When using the command line, the following can be used to specify how to load
the application and to set the correct environment for development:

.. code-block:: bash

  export FLASK_APP=${HOME}/workspace/kadi/kadi/wsgi.py
  export FLASK_ENV=development

Alternatively when using the :file:`.env` file instead:

.. code-block:: none

  FLASK_APP=${HOME}/workspace/kadi/kadi/wsgi.py
  FLASK_ENV=development

For the development environment all configuration values have default values
set which correspond to the configuration values used in this documentation, so
no further changes should be necessary. However, if any of those values need to
be changed regardless, it is best done using the ``KADI_CONFIG_FILE``
environment variable. This variable needs to point to a valid configuration
Python file (which also needs to be created first) in which the desired values
can be specified. This file can be treated like a normal Python file, i.e.
calculating values or importing other modules will work, however, the
syntactical rules and formatting of Python also apply. For example, the
following can be used on the command line assuming the specified file is our
configuration file:

.. code-block:: bash

  export KADI_CONFIG_FILE=${HOME}/workspace/kadi/config/config.py

Similarly, the same can again be specified in the :file:`.env` file instead:

.. code-block:: none

  KADI_CONFIG_FILE=${HOME}/workspace/kadi/config/config.py

See how to :ref:`configure Kadi4Mat in a production environment
<installation-production-manual-configuration-kadi4mat>` for an example of how
some configuration values can be overridden.

Setting up the application
--------------------------

Before the application can be used, some initialization steps have to be done
using the Kadi command line interface (CLI):

.. code-block:: bash

  kadi assets dev   # Install the frontend dependencies and build the assets
  kadi db init      # Initialize the database
  kadi search init  # Initialize the search indices
  kadi i18n compile # Compile the backend translations

The Kadi CLI offers some useful tools and utility functions running in the
context of the application (see also :ref:`Command line interfaces
<development-general-cli>`). As such, it also needs access to the correct
configuration specified by the environment variables explained in the previous
section. When using the :file:`.env` file instead, this means that the commands
have to be run inside the directory that contains this file (or in any child
directory, actually).

Another useful command when setting up the application for the first time is
the following one, which can be used to set up some initial local dummy users
and resources:

.. code-block:: bash

  kadi db test-data

The three main dummy users useful for testing are:

========   =========   =================================
Username   Password    System Role
========   =========   =================================
admin      admin123    Admin  (Can manage any resource)
member     member123   Member (Can create new resources)
guest      guest123    Guest  (Read only access)
========   =========   =================================

Running the application
-----------------------

Flask includes a lightweight development server, which can be run using the
Flask CLI (see also :ref:`Command line interfaces <development-general-cli>`):

.. code-block:: bash

  flask run

When using the :file:`.env` file and the application cannot be found, make sure
to run the command in the correct directory. Afterwards the application should
run locally on port 5000:

.. code-block:: bash

  firefox http://localhost:5000

To be able to run asynchronous background tasks with Celery (needed for example
when uploading files or sending emails), the following command can be used:

.. code-block:: bash

  kadi celery worker -B --loglevel=INFO

This will start the normal Celery worker as well as Celery beat, which is used
for periodic tasks (e.g. for deleting expired uploads) in a single process,
which is convenient for development.

To "send" emails without using an actual SMTP server, the following can be used
to simply print the emails on the terminal instead (this debugging server is
already configured as default when developing):

.. code-block:: bash

  python -m smtpd -n -c DebuggingServer localhost:8025

Updating the application
------------------------

After updating the application code via git, the following commands may have to
be run again:

.. code-block:: bash

  pip install -e ${HOME}/workspace/kadi[dev] # Install any new backend dependencies
  kadi assets dev                            # Install any new frontend dependencies and rebuild the assets
  kadi db upgrade                            # Upgrade the database schema
  kadi i18n compile                          # Recompile any new backend translations
